// define variable for ball count paragraph

const PARA = document.querySelector('p');
let count = 0;

// setup canvas

const CANVAS = document.querySelector('canvas');
const CTX = CANVAS.getContext('2d');

let width = CANVAS.width = window.innerWidth;
let height = CANVAS.height = window.innerHeight;

// function to generate random number

function random(min,max) {
    const NUM = Math.floor(Math.random()*(max-min)) + min;
    return NUM;
}

// define shape constructor and methods

class Shape {
    constructor(x, y, velX, velY, exists) {
        this.x = x;
        this.y = y;
        this.velX = velX;
        this.velY = velY;
        this.exists = exists;
    }
}

// define Ball constructor and methods, inheriting from Shape

class Ball extends Shape {
    constructor(x, y, velX, velY, exists, color, size) {
        super(x, y, velX, velY, exists);

        this.color = color;
        this.size = size;
    }

    draw() {
        CTX.beginPath();
        CTX.fillStyle = this.color;
        CTX.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
        CTX.fill();
    }

    update() {
        if ((this.x + this.size) >= width) {
            this.velX = -(this.velX);
        }

        if ((this.x - this.size) <= 0) {
            this.velX = -(this.velX);
        }

        if ((this.y + this.size) >= height) {
            this.velY = -(this.velY);
        }

        if ((this.y - this.size) <= 0) {
            this.velY = -(this.velY);
        }

        this.x += this.velX;
        this.y += this.velY;
    }

    collisionDetect() {
        for (let j = 0; j < balls.length; j++) {
            if (!(this === balls[j])) {
                let dx = this.x - balls[j].x;
                let dy = this.y - balls[j].y;
                let distance = Math.sqrt(dx * dx + dy * dy);

                if (distance < this.size + balls[j].size) {
                    balls[j].color = this.color = `rgb(${random(0, 255)},${random(0, 255)},${random(0, 255)})`;
                }
            }
        }
    }
}

Ball.prototype = Object.create(Shape.prototype);
Ball.prototype.constructor = Ball;

// define EvilCircle constructor and methods, inheriting from Shape

class EvilCircle extends Shape {
    constructor(x, y, velX, velY, exists, color, size) {
        super(x, y, 20, 20, exists);

        this.color = 'white';
        this.size = 10;
    }

    draw() {
        CTX.beginPath();
        CTX.strokeStyle = this.color;
        CTX.lineWidth = 3;
        CTX.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
        CTX.stroke();
    }

    checkBounds() {
        if ((this.x + this.size) >= width) {
            this.x -=(this.size);
        }

        if ((this.x - this.size) <= 0) {
            this.x += (this.size);
        }

        if ((this.y + this.size) >= height) {
            this.y -=(this.size);
        }

        if ((this.y - this.size) <= 0) {
            this.y +=(this.size);
        }
    }

    setControls() {
        let _this = this;
        window.onkeydown = function(e) {
            if (e.keyCode === 65 || e.keyCode === 37) {
                _this.x -= _this.velX;
            } else if (e.keyCode === 68 || e.keyCode === 39) {
                _this.x += _this.velX;
            } else if (e.keyCode === 87 || e.keyCode === 38) {
                _this.y -= _this.velY;
            } else if (e.keyCode === 83 || e.keyCode === 40) {
                _this.y += _this.velY;
            }
        }
    }

    collisionDetect() {
        for(let j = 0; j < balls.length; j++) {
            if( balls[j].exists ) {
                let dx = this.x - balls[j].x;
                let dy = this.y - balls[j].y;
                let distance = Math.sqrt(dx * dx + dy * dy);

                if (distance < this.size + balls[j].size) {
                    balls[j].exists = false;
                    count--;
                    PARA.textContent = 'Ball count: ' + count;
                }
            }
        }
    }
}

EvilCircle.prototype = Object.create(Shape.prototype);
EvilCircle.prototype.constructor = EvilCircle;

let balls = [];

let evil = new EvilCircle(random(0,width), random(0,height), true);
evil.setControls();

// define loop that keeps drawing the scene constantly

function loop() {
    CTX.fillStyle = 'rgba(0, 0, 0, 0.25)';
    CTX.fillRect(0, 0, width, height);

    while (balls.length < 25) {
        let size = random(10,20);
        let ball = new Ball(
            // ball position always drawn at least one ball width
            // away from the edge of the canvas, to avoid drawing errors
            random(0 + size,width - size),
            random(0 + size,height - size),
            random(-7,7),
            random(-7,7),
            true,
            `rgb(${random(0,255)},${random(0,255)},${random(0,255)})`,
            size
        );
        balls.push(ball);
        count++;
        PARA.textContent = 'Ball count: ' + count;
    }

    for (let i = 0; i < balls.length; i++) {
        if(balls[i].exists) {
            balls[i].draw();
            balls[i].update();
            balls[i].collisionDetect();
        }
    }

    evil.draw();
    evil.checkBounds();
    evil.collisionDetect();

    requestAnimationFrame(loop);
}

loop();